<?php
    //include_once(CLASS_PATH."/aircraft.inc");
    require_once '../../resources/config.php';
    require(CLASS_PATH."/aircraft_type.inc");
	
    session_start();
    $pilot_id = $_SESSION['pilot_id'];
	$craft_iata = $_POST["iata"];
	$craft_name = $_POST["name"];
	$craft_isMulti = $_POST["isMulti"];
	$craft_isComplex = $_POST["isComplex"];
	$craft_isFixedWing = $_POST["isFixedWing"];
	$craft_isLand = $_POST["isLand"];
    
			
    $type = new aircraft_type();
	$type->type_iata = $craft_iata;
	$type->type_name = $craft_name;
	$type->type_is_multi = $craft_isMulti;
	$type->type_is_complex = $craft_isComplex;
	$type->type_land = $craft_isLand;
	$type->type_fixed_wing = $craft_isFixedWing;
    $type->pilot_id = $pilot_id;
	
	$type->insert_yourself();
?>
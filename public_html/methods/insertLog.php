<?php
    require_once '../../resources/config.php';
    require(CLASS_PATH."/logbook.inc");
	
    session_start();
    $pilot_id = $_SESSION['pilot_id'];
    $craft_id = $_POST["craft_id"];
    $log_from_ICAO = $_POST["log_from_ICAO"];
    $log_to_ICAO = $_POST["log_to_ICAO"];
    $log_out = $_POST["log_out"];
    $log_off = $_POST["log_off"];
    $log_on = $_POST["log_on"];
    $log_in = $_POST["log_in"];
    $log_function = $_POST["log_function"];
    $log_flight_rules = $_POST["log_flight_rules"];
    $log_TO = $_POST["log_TO"];
    $log_LAND = $_POST["log_LAND"];
    $log_app = $_POST["log_app"];
    $log_comm = $_POST["log_comm"];
    $log_flt_no = $_POST["log_flt_no"];
    $log_flt_duty = $_POST["log_flt_duty"];


            
    $log = new logbook();
    
    $log->craft_id = $craft_id;
    $log->pilot_id = $pilot_id;
    $log->log_from_ICAO = $log_from_ICAO;
    $log->log_to_ICAO = $log_to_ICAO;
    $log->log_out = $log_out;
    $log->log_off = $log_off;
    $log->log_on = $log_on;
    $log->log_in = $log_in;
    $log->log_function = $log_function;
    $log->log_flight_rules = $log_flight_rules;
    $log->log_TO = $log_TO;
    $log->log_LAND = $log_LAND;
    $log->log_app = $log_app;
    $log->log_comm = $log_comm;
    $log->log_flt_no = $log_flt_no;
    $log->log_flt_duty = $log_flt_duty;

    $log->insert_yourself();
?>
<?php
include_once("../resources/config.php");
include_once("methods.inc");
include_once("snippets.inc");
include_once(CLASS_PATH."/aircraft_type.inc");
include_once(CLASS_PATH."/aircraft.inc");
?>
<head>
	<?php add_css_screen();?>

</head>

<body>
    <script type="text/javascript"  src="js/jquery-2.1.0.js"></script>
    <script type="text/javascript"  src="js/jquery.cookie.js"></script>
    <script type="text/javascript">
        $(document).keyup(function(e) {
          if (e.keyCode == 27) { //Si es escape, cierro todos los forms
            $('#aircraftForm').hide('fast');
            $('#typeForm').hide('fast');
            $('#loginForm').hide('fast');
            $('#logbookForm').hide('fast'); 
          }
        });
        window.onload = function(){
            if($.cookie('tabCraft') == 'true'){$('#tabcraft').show()}
            if($.cookie('tabType') == 'true'){$('#tabtypes').show()}
            if($.cookie('tabLogbook') == 'true'){$('#tabLogbook').show()}
        }
        
        /*DELETE FUNCTIONS*/
        function deleteAircraft(craft_id) {
            var didConfirm = confirm("Are you sure you want to delete this Aircraft?");
            if (didConfirm == true) {
                $.ajax({
                    type:"POST",
                    url:"methods/deleteAircraft.php",
                    data: {id:craft_id},
                    success: function(){location.reload()}
                });
                
                //Actualizo cookies para mantener el estado de la pagina por si se refresca
                $.cookie('tabType',false); //saco la tabla de Tipos
                $.cookie('tabLogbook',false);   //saco la tabla del libro
                $.cookie('tabCraft',true);   //pongo la tabla de Aviones
                //=================
            }
            
        }
        function deleteType(type_id) {
            var didConfirm = confirm("Are you sure you want to delete this Type?");
            if (didConfirm == true) {
                $.ajax({
                    type:"POST",
                    url:"methods/deleteType.php",
                    data: {id:type_id},
                    success: function(){location.reload()}
                });
                
                //Actualizo cookies para mantener el estado de la pagina por si se refresca
                $.cookie('tabCraft',false); //saco la tabla de Aircraft
                $.cookie('tabLogbook',false);   //saco la tabla del libro
                $.cookie('tabType',true);   //pongo la tabla de Tipos
                //=================
            }
        }
        function deleteLogEntry(log_id) {
            var didConfirm = confirm("Are you sure you want to delete this log?");
            if (didConfirm == true) {
                $.ajax({
                    type:"POST",
                    url:"methods/deleteLog.php",
                    data: {id:log_id},
                    success: function(){location.reload()}
                });
                
                //Actualizo cookies para mantener el estado de la pagina por si se refresca
                $.cookie('tabCraft',false); //saco la tabla de Tipos
                $.cookie('tabType',false);   //saco la tabla de Aviones
                $.cookie('tabLogbook',true);   //pongo la tabla del libro
                //=================
            }
        }
        
        /*INSERT FUNCTIONS*/
        function insertType() {
            var craft_iata = $('[name="edIata"]').val();
            var craft_name = $('[name="edName"]').val();
            var craft_isMulti = $('[name="isMulti"]').val();
            var craft_isComplex = $('[name="isComplex"]').val();
            var craft_isFixedWing = $('[name="isFixedWing"]').val();
            var craft_isLand = $('[name="isLand"]').val();
         	$.ajax({
                type:"POST",
                url:"methods/insertType.php",
                data: {iata:craft_iata, name:craft_name, isMulti:craft_isMulti, isComplex:craft_isComplex, isFixedWing:craft_isFixedWing, isLand:craft_isLand},
                success: function (){location.reload()}
            });
            //Actualizo cookies para mantener el estado de la pagina por si se refresca
                $.cookie('tabCraft',false); //saco la tabla de Tipos
                $.cookie('tabLogbook',false);   //saco la tabla del libro
                $.cookie('tabType',true);   //pongo la tabla de Aviones
                //=================
        }
        function insertAircraft() {
            var craft_type = $('[name="crType"]').val();
            var craft_reg = $('[name="edReg"]').val();
            var craft_marks = $('[name="edMarks"]').val();
            var craft_comm = $('[name="edComm"]').val();
         	$.ajax({
                type:"POST",
                url:"methods/insertCraft.php",
                data: {type:craft_type, reg:craft_reg, marks:craft_marks, comm:craft_comm},
                success: function (){location.reload()}
            });
            //Actualizo cookies para mantener el estado de la pagina por si se refresca
            $.cookie('tabType',false); //saco la tabla de Tipos
            $.cookie('tabLogbook',false);   //saco la tabla del libro
            $.cookie('tabCraft',true);   //pongo la tabla de Aviones
            //=================
        }
        function insertLog() {

            var craft_id  = $('[name="logbookAircraftDDL"]').val();
            //var pilot_id  = $('[name="logbookPilotsDDL"]').val();
            var log_from_ICAO  = $('[name="edFromIATA"]').val();
            var log_to_ICAO = $('[name="edTOIATA"]').val();
            var log_out = $('[name="edLogOUT"]').val();
            var log_off = $('[name="edLogOFF"]').val();
            var log_on = $('[name="edLogON"]').val();
            var log_in = $('[name="edLogIN"]').val();
            var log_function = $('[name="functionOnboardDDL"]').val();
            var log_flight_rules = $('[name="flightRulesDDL"]').val();
            var log_TO = $('[name="edNoTO"]').val();
            var log_LAND = $('[name="edNoLAND"]').val();
            var log_app = $('[name="ApproachTypeDDL"]').val();
            var log_comm = $('[name="logbookCommentTA"]').val();
            var log_flt_no = $('[name="edFltNo"]').val();
            var log_flt_duty = $('[name="flightDutyDDL"]').val();
            
            
            
         	$.ajax({
                type:"POST",
                url:"methods/insertLog.php",
                data: {
                    craft_id: craft_id,
                    //pilot_id:pilot_id,
                    log_from_ICAO:log_from_ICAO,
                    log_to_ICAO:log_to_ICAO,
                    log_out:log_out,
                    log_off:log_off,
                    log_on:log_on,
                    log_in:log_in,
                    log_function:log_function,
                    log_flight_rules:log_flight_rules,
                    log_TO:log_TO,
                    log_LAND:log_LAND,
                    log_app:log_app,
                    log_comm:log_comm,
                    log_flt_no:log_flt_no,
                    log_flt_duty:log_flt_duty
                },
                success: function (){location.reload()}
            });
            //Actualizo cookies para mantener el estado de la pagina por si se refresca
                $.cookie('tabCraft',false); //saco la tabla de Tipos
                $.cookie('tabType',false);   //saco la tabla de Aviones
                $.cookie('tabLogbook',true);   //pongo la tabla del libro
                //=================
        }
        
        /*SHOW/HIDE DISPLAY DIVS - TABLES*/
        function showAircraftTable(){
            //escondo otras cosas
            $('#tabtypes').hide('fast');
            $('#loginForm').hide('fast');
            $('#tabLogbook').hide('fast');
            
            if($('#tabcraft').css('display') == 'none'){ 
               $('#tabcraft').show('fast');
                
            } else { 
               $('#tabcraft').hide('fast'); 
            }
        }
        function showTypeTable(){
            //escondo otras cosas
            $('#tabcraft').hide('fast');
            $('#loginForm').hide('fast');
            $('#tabLogbook').hide('fast');
            
            if($('#tabtypes').css('display') == 'none'){
                $('#tabtypes').show('fast'); 
            } else { 
                $('#tabtypes').hide('fast'); 
            }
        }
        function showLogbookTable(){
            //escondo otras cosas
            $('#tabcraft').hide('fast');
            $('#loginForm').hide('fast');
            $('#tabtypes').hide('fast');
            
            if($('#tabLogbook').css('display') == 'none'){
                $('#tabLogbook').show('fast'); 
            } else { 
                $('#tabLogbook').hide('fast'); 
            }
        }
        
        /*SHOW/HIDE FORMS*/
        function showTypeForm(){
            
            //escondo los otros
            $('#aircraftForm').hide('fast'); 
            $('#loginForm').hide('fast'); 
            
            if($('#typeForm').css('display') == 'none'){ 
               $('#typeForm').show('fast'); 
            } else { 
               $('#typeForm').hide('fast'); 
            }
        }
        function showLoginForm(){
            $('#aircraftForm').hide('fast'); 
            $('#typeForm').hide('fast'); 
            $('#logbookForm').hide('fast');
            
            if($('#loginForm').css('display') == 'none'){ 
               $('#loginForm').show('fast'); 
            } else { 
               $('#loginForm').hide('fast'); 
            }
        }
        function showLogbookForm(){
            $('#aircraftForm').hide('fast'); 
            $('#typeForm').hide('fast'); 
            
            if($('#logbookForm').css('display') == 'none'){ 
               $('#logbookForm').show('fast'); 
            } else { 
               $('#logbookForm').hide('fast'); 
            }
        }
        function showAircraftForm(){
            //escondo los otros forms
            $('#typeForm').hide('fast');
            $('#loginForm').hide('fast'); 
            
            if($('#aircraftForm').css('display') == 'none'){ 
               $('#aircraftForm').show('fast'); 
            } else { 
               $('#aircraftForm').hide('fast'); 
            }
        }
        
        /*CONTROL FUNCTIONS*/
        function doCheckUser(){
            var acc_user = $('[name="edUser"]').val();
            var acc_pass = $('[name="edPass"]').val();
            
            $.ajax({
                type:"POST",
                url:"methods/checkUser.php",
                data: {acc_user:acc_user,acc_pass:acc_pass},
                success: function (){location.reload()}
            });
               //Actualizo cookies para mantener el estado de la pagina por si se refresca
               // $.cookie('tabCraft',false); //saco la tabla de Tipos
                //$.cookie('tabLogbook',false);   //saco la tabla del libro
                //$.cookie('tabType',true);   //pongo la tabla de Aviones
                //=================
        }
        function doLogout(){
            
            $.ajax({
                type:"POST",
                url:"methods/doLogout.php",
                data: {},
                success: function (){location.reload()}
            });
        }
        
    </script>
    
    <div class="menu">
        <input type="button" class="menu" id="button_menu" value="Types" onclick="showTypeTable()">
        <input type="button" class="menu" id="button_menu" value="Aircrafts" onclick="showAircraftTable()">
        <input type="button" class="menu" id="button_menu" value="Logbook" onclick="showLogbookTable()">
        <input type="button" class="menu" id="button_menu" value="Reports" onclick="showReportsDiv()">
        <input type="button" class="menu" id="button_login" value="Login" onClick="showLoginForm()">
        <?php
            session_start();
            if (!empty($_SESSION['pilot_id'])){
                $session_pilot_id = $_SESSION['pilot_id'];
                $session_pilot_name = $_SESSION['pilot_name'];
                echo "<div class='menu' id='user_logged'>User Logged: $session_pilot_name</div>";
            } else { 
                echo "<div class='menu' id='user_logged'>User Logged: None</div>";
                $session_pilot_id = 0;
                $session_pilot_name = "";
            }
        ?>
        
        <div class="callout_form" id="loginForm">
            <form>
                <?php
                    if ($session_pilot_id) { //si hay alguien logueado le ofrezco desloguear si no, el login
                        echo '<input type="button" id="button" value="Logout" name="btnLogin" onclick="doLogout()">';}
                    else {
                        echo '<input type="text" id="textbox" placeholder="username" name="edUser">';
                        echo '<input type="password" id="textbox" placeholder="password" name="edPass">';
                        echo '<input type="button" id="button" value="Login" name="btnLogin" onclick="doCheckUser()">';
                        echo '<a href="javascript:void(0);" onclick="doRegister()">Register</a>';
                         }  
                ?>
                
            </form>
        </div>  
        
        <div class="display" id="tabcraft">
            <div class="callout_form" id="aircraftForm">

                <form method="post" action="#">
                    Type:
                    <select name="crType" id="dropdownlist">
                    <?php
                        $query = "SELECT * FROM ".DB_NAME.".aircraft_type WHERE pilot_id = $session_pilot_id;";
                        $arr = execSQL($query);
                        $i = 1;
                        foreach ($arr as $row) {
                            ($i % 2)== 0 ? $class = "grid_evenrow" : $class = "grid_oddrow";
                            $type_id = $row['type_id'];
                            $type_iata = $row['type_iata'];
                            $type_name = $row['type_name'];
                            echo "<option value='$type_id'>$type_iata - $type_name</option>";
                        }
                    ?>
                    </select>
                    <br>
                    <input id='textbox' type="text" placeholder="Registration, eg: N123LA" name="edReg"/>
                    <br>
                    <input id='textbox' type="text" placeholder="Markings, eg: White and Red" name="edMarks"/>
                    <br>
                    <textarea cols="30" rows="10" id="freetext" name="edComm" placeholder="comments"></textarea>
                    <br>
                    <input type="button" name="submit" id="button" value="Insertar" onclick="insertAircraft()">
                </form>	
            </div>
            <table class="grid" id="aircraftsTable">
            <tr>
                <th class='grid'>Aircraft id</th>
                <th class='grid'>Aircraft Type id</th>
                <th class='grid'>Aircraft Registration</th>
                <th class='grid'>Type IATA</th>
                <th class='grid'>Type Name</th>
                <th class='grid'>Aircraft Markings</th>
                
                <?php 
                    if ($session_pilot_id) {
                        echo '<th class="grid"><a href="javascript:void(0);" onclick="showAircraftForm()" class="add">+</a></th>';
                    }else{
                        echo '<th class="grid"></th>';
                    }
                ?>

                
            </tr>

                <?php

                $query = "SELECT * FROM (".DB_NAME.".aircraft as a, ".DB_NAME.".aircraft_type as t)
                WHERE a.type_id = t.type_id
                AND a.pilot_id = $session_pilot_id;";

                $arr = execSQL($query);
                $i = 1;
                foreach ($arr as $row) {
                    ($i % 2)== 0 ? $class = "grid_evenrow" : $class = "grid_oddrow";
                    $craft_id = $row['craft_id'];
                    $type_id = $row['type_id'];
                    $craft_reg = $row['craft_reg'];
                    $craft_markings = $row['craft_markings'];
                    $type_iata = $row['type_iata'];
                    $type_name = $row['type_name'];
                    echo "<td class='$class'>$craft_id</td>";
                    echo "<td class='$class'>$type_id</td>";
                    echo "<td class='$class'>$craft_reg</td>";
                    echo "<td class='$class'>$type_iata</td>";
                    echo "<td class='$class'>$type_name</td>";
                    echo "<td class='$class'>$craft_markings</td>";
                    echo "<td class='$class'><a class='rowdel' onclick='deleteAircraft($craft_id)' href='javascript:void(0);'>delete</a></td>";
                    echo"</tr>";
                    $i++;
                }
            ?>

            </table>
</div>
        <div class="display" id="tabtypes">
            <div class="callout_form" id="typeForm">
                <form method="post" action="#">
                    <input id='textbox' type="text" placeholder="IATA, eg: B763" name="edIata"/>
                    <br>
                    <input id='textbox' type="text" placeholder="Dsc, eg: Boeing 767-300" name="edName"/>
                    <br>
                    Multi:
                    <select name="isMulti" id="dropdownlist">
                        <option id="selectbox" value="Y">YES</option>
                        <option id="selectbox" value="N">NO</option>
                    </select>
                    <br>
                    Complex:
                    <select name="isComplex" id="dropdownlist">
                        <option id="selectbox" value="Y">YES</option>
                        <option id="selectbox" value="N">NO</option>
                    </select>
                    <br>
                    Fixed Wing:
                    <select name="isFixedWing" id="dropdownlist">
                        <option id="selectbox" value="Y">YES</option>
                        <option id="selectbox" value="N">NO</option>
                    </select>
                    <br>
                    Land/Water:
                    <select name="isLand" id="dropdownlist">
                        <option id="selectbox" value="Y">LAND</option>
                        <option id="selectbox" value="N">WATER</option>
                        <option id="selectbox" value="B">BOTH</option>
                    </select>
                    <input type="button" name="submit" id="button" value="Insertar" onclick="insertType()">
                </form>	
            </div>
            
            <table class="grid" id="typesTable">
            <tr>
                <th class='grid'>Type id</th>
                <th class='grid'>Type IATA</th>
                <th class='grid'>Type Name</th>
                <th class='grid'>Type Is Multi</th>
                <th class='grid'>Type Is Fixed Wing</th>
                <th class='grid'>Type is Complex</th>
                <th class='grid'>Type is Land</th>
                <?php 
                    if ($session_pilot_id) {
                        echo '<th class="grid"><a href="javascript:void(0);" onclick="showTypeForm()" class="add">+</a></th>';
                    }else{
                        echo '<th class="grid"></th>';
                    }
                ?>
                
            </tr>

                <?php
                 "SELECT * FROM ".DB_NAME.".aircraft_type WHERE pilot_id = $session_pilot_id;";

                $arr = execSQL($query);
                $i = 1;
                foreach ($arr as $row) {
                    ($i % 2)== 0 ? $class = "grid_evenrow" : $class = "grid_oddrow";

                    $type_id = $row['type_id'];
                    $type_iata = $row['type_iata'];
                    $type_name = $row['type_name'];
                    $type_is_multi = $row['type_is_multi'];
                    $type_is_complex = $row['type_is_complex'];
                    $type_fixed_wing = $row['type_fixed_wing'];
                    $type_land = $row['type_land'];

                    echo "<td class='$class'>$type_id</td>";
                    echo "<td class='$class'>$type_iata</td>";
                    echo "<td class='$class'>$type_name</td>";
                    echo "<td class='$class'>$type_is_multi</td>";
                    echo "<td class='$class'>$type_fixed_wing</td>";
                    echo "<td class='$class'>$type_is_complex</td>";
                    echo "<td class='$class'>$type_land</td>";
                    echo "<td class='$class'><a class='rowdel' onclick='deleteType($type_id)' href='javascript:void(0);'>delete</a></td>";
                    echo"</tr>";
                    $i++;
                }
            ?>
             </table>   

</div>
        <div class="display" id="tabLogbook">
            <div class="callout_form" id="logbookForm">
                <form method="post" action="#">
                    <input id='textbox' type="text" placeholder="Flight #" name="edFltNo"/>
                    Aircraft:
                    <select name="logbookAircraftDDL" id="dropdownlist">
                    <?php
                        $query = "SELECT * FROM (".DB_NAME.".aircraft as a, ".DB_NAME.".aircraft_type as t) 
                        WHERE t.type_id = a.type_id
                        AND $session_pilot_id = a.pilot_id;";
                        $arr = execSQL($query);
                        foreach ($arr as $row) {
                            $craft_id = $row['craft_id'];
                            $craft_reg = $row['craft_reg'];
                            $type_name = $row['type_name'];
                            echo "<option value='$craft_id'>$craft_reg - $type_name</option>";
                        }
                    ?>
                    </select>
                    <input id='textbox' type="text" placeholder="FROM, eg. KLAX" name="edFromIATA"/>
                    <input id='textbox' type="text" placeholder="TO, eg. KSFO" name="edTOIATA"/>
                    <input id='textbox' type="text" placeholder="OUT: YYYY-MM-DD HH:MM:SS" name="edLogOUT"/>
                    <input id='textbox' type="text" placeholder="OFF: YYYY-MM-DD HH:MM:SS" name="edLogOFF"/>
                    <input id='textbox' type="text" placeholder="ON: YYYY-MM-DD HH:MM:SS" name="edLogON"/>
                    <input id='textbox' type="text" placeholder="IN: YYYY-MM-DD HH:MM:SS" name="edLogIN"/>
                    Function Onboard:
                    <select name="functionOnboardDDL" id="dropdownlist">
                        <option value="PIC" selected="selected">PIC</option>
                        <option value="SIC">SIC</option>
                        <option value="STU">Student</option>
                        <option value="INS">Instructor</option>
                    </select>
                    Flight Duty:
                    <select name="flightDutyDDL" id="dropdownlist">
                        <option value="FLT" selected="selected">Flight</option>
                        <option value="DHD">Dead Head</option>
                        <option value="SIM">Simulator</option>
                    </select>
                    Flight Rules:
                    <select name="flightRulesDDL" id="dropdownlist">
                        <option value="VFR" selected="selected">VFR</option>
                        <option value="IFR">IFR</option>
                    </select>
                    <input id='textbox' type="text" placeholder="Number of T/O" name="edNoTO"/>
                    <input id='textbox' type="text" placeholder="Number of Landings" name="edNoLAND"/>
                    Approach Type:
                    <select name="ApproachTypeDDL" id="dropdownlist">
                        <option value="" selected="selected">None</option>
                        <option value="ILSI">ILS CATI</option>
                        <option value="ILSII">ILS CATII</option>
                        <option value="ILSIII">ILS CATIII</option>
                        <option value="VOR">VOR</option>
                        <option value="VORD">VOR-DME</option>
                        <option value="NDB">NDB</option>
                    </select>
                    <textarea id="freetext" name="logbookCommentTA" placeholder="Comments"></textarea>
                    <input type="button" name="submit" id="button" value="Insert" onclick="insertLog()">
                </form>
            </div>
            <table class="grid" id="logblookTable">
            <tr>
                <!--<th class='grid'>Aircraft Id</th>
                <th class='grid'>Pilot Id</th>-->
                <th class='grid'>Flt.#</th>
                <th class='grid'>Airplane</th>
                <th class='grid'>Register</th>
                <th class='grid'>FROM</th>
                <th class='grid'>TO</th>
                <th class='grid'>OUT</th>
                <!--<th class='grid'>OFF</th>
                <th class='grid'>ON</th>-->
                <th class='grid'>IN</th>
                <th class='grid'>Function</th>
                <th class='grid'>Duty</th>
                <th class='grid'>Flight Rules</th>
                <th class='grid'>#T/O</th>
                <th class='grid'>#Landings</th>
                <th class='grid'>Approach</th>
                <th class='grid'>Comments</th>
                
                <?php 
                    if ($session_pilot_id) {
                        echo '<th class="grid"><a href="javascript:void(0);" onclick="showLogbookForm()" class="add">+
                        </a></th>';
                    }else{
                        echo '<th class="grid"></th>';
                    }
                ?>
                
                
            </tr>

                <?php
                $query = "SELECT * FROM (".DB_NAME.".log as l, ".DB_NAME.".aircraft as a, ".DB_NAME.".aircraft_type as t) 
                WHERE l.craft_id = a.craft_id 
                AND a.type_id = t.type_id
                AND l.pilot_id = $session_pilot_id
                ORDER BY log_out;";

                $arr = execSQL($query);
                $i = 1;
                foreach ($arr as $row) {
                    ($i % 2)== 0 ? $class = "grid_evenrow" : $class = "grid_oddrow";
                    
                    echo "<tr>";
                    $log_id = $row['log_id'];
                    $craft_id = $row['craft_id'];
                    $pilot_id = $row['pilot_id'];
                    $log_flt_no = $row['log_flt_no'];
                    $craft_reg = $row['craft_reg'];
                    $type_name = $row['type_name'];
                    $log_from_ICAO = $row['log_from_ICAO'];
                    $log_to_ICAO = $row['log_to_ICAO'];
                    $log_out = $row['log_out'];
                    $log_off = $row['log_off'];
                    $log_on = $row['log_on'];
                    $log_in = $row['log_in'];
                    $log_function = $row['log_function'];
                    $log_flt_duty = $row['log_flt_duty'];
                    $log_flight_rules = $row['log_flight_rules'];
                    $log_TO = $row['log_TO'];
                    $log_LAND = $row['log_LAND'];
                    $log_app = $row['log_app'];
                    $log_comm = $row['log_comm'];


                    //echo "<td class='$class'>$craft_id</td>";
                    //echo "<td class='$class'>$pilot_id</td>";
                    echo "<td class='$class'>$log_flt_no</td>";
                    echo "<td class='$class'>$type_name</td>";
                    echo "<td class='$class'>$craft_reg</td>";
                    
                    echo "<td class='$class'>$log_from_ICAO</td>";
                    echo "<td class='$class'>$log_to_ICAO</td>";
                    echo "<td class='$class'>$log_out</td>";
                    //echo "<td class='$class'>$log_off</td>";
                    //echo "<td class='$class'>$log_on</td>";
                    echo "<td class='$class'>$log_in</td>";
                    echo "<td class='$class'>$log_function</td>";
                    echo "<td class='$class'>$log_flt_duty</td>";
                    echo "<td class='$class'>$log_flight_rules</td>";
                    echo "<td class='$class'>$log_TO</td>";
                    echo "<td class='$class'>$log_LAND</td>";
                    echo "<td class='$class'>$log_app</td>";
                    echo "<td class='$class'>$log_comm</td>";
                    
                    echo "<td class='$class'><a class='rowdel' onclick='deleteLogEntry($log_id)' href='javascript:void(0);'>delete</a></td>";
                    echo"</tr>";
                    $i++;
                }
            ?>
             </table> 
            
        </div>
        
    </div>
    
</body>
<?php 
/* Contains the logic for Logbook
 * A class that handles the logic of manipulating the logbook
 * 
 * @author Daniel Waksman
 * @license http://opensource.org/licenses/GPL-3.0  GNU Public Version 3
 * */

class logbook{
	
	var $log_id;			//LOG ID
	var $craft_id;			//Aircraft that flown that leg
	var $pilot_id;			//Pilot who owns the log
	var $log_from_ICAO;		//FROM ICAO eg:SUCD
	var $log_to_ICAO;		//TO ICAO	eg:SUMU
	var $log_out;			//Pushback: Datetime 'Y-m-d H:i:s'  2005-03-17 16:45:00
	var $log_off;			//Takoff: Datetime 'Y-m-d H:i:s'  2005-03-17 16:45:00
	var $log_on;			//Landing: Datetime 'Y-m-d H:i:s'  2005-03-17 16:45:00
	var $log_in;			//Shutdown: Datetime 'Y-m-d H:i:s'  2005-03-17 16:45:00
	var $log_function;		//Function on board PIC/SIC
	var $log_flight_rules;	//IFR/VFR
	var $log_TO;			//Number of takoffs in that leg
	var $log_LAND;			//Number of landings in that leg 
	var $log_app;			//Type of approach ILSI,ILSII,ILSIII,VOR,VORD,TACAN,NDB,GPS
	var $log_comm;			//Any other comment
    
    const TABLE = "log";

	/*Generates the insert sentence for the data instanciated in the structure
	 * @return string $query
	 * @author Daniel Waksman
	 * */
	function gen_SQL_insert(){
		$query = "INSERT INTO ".DB_NAME.".".$this::TABLE."(
		craft_id,
		pilot_id,
		log_from_ICAO,
		log_to_ICAO,
		log_out,
		log_off,
		log_on,
		log_in,
		log_function,
		log_flight_rules,
		log_TO,
		log_LAND,
		log_app,
		log_comm,
        log_flt_no,
        log_flt_duty
		) 
		VALUES (
		'$this->craft_id',
		'$this->pilot_id',
		'$this->log_from_ICAO',
		'$this->log_to_ICAO',
		'$this->log_out',
		'$this->log_off',
		'$this->log_on',
		'$this->log_in',
		'$this->log_function',
		'$this->log_flight_rules',
		'$this->log_TO',
		'$this->log_LAND',
		'$this->log_app',
		'$this->log_comm',
        '$this->log_flt_no',
        '$this->log_flt_duty'
		);";
		return $query;
	}
    
	/* Generates SQL sentence to delete the data instanciated in the structure
	 * @return string $query
	 * @author Daniel Waksman
	 * */
	function gen_SQL_delete(){
		$id = $this->log_id;
		$query = "DELETE FROM ".DB_NAME.".".$this::TABLE." WHERE log_id = $id;";
		return $query;
	}	

	/* Insert the data instanciated in the structure into de database
	 * @author Daniel Waksman
	 * */
	function insert_yourself(){
		$query = $this->gen_SQL_insert();
		require_once 'methods.inc';
		execSQL_Insert($query);
	}
	
	/* Deletes an aircraft as the ID in the structure
	 * @author Daniel Waksman
	 * */
	function delete_yourself(){
		$query = $this->gen_SQL_delete();
		require_once 'methods.inc';
		execSQL_Insert($query);
	}
     
}


?>
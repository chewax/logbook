<?php 
/* Contains the logic for all Aircraft
 * A class that handles the logic of manipulating Aircrafts Types
 * 
 * @author Daniel Waksman
 * @license http://opensource.org/licenses/GPL-3.0  GNU Public Version 3
 * */

class aircraft_type{
	
    var $type_id;          //aircraft id: autoincrement
    var $type_iata;        //aircraft IATA code. eg: B763
    var $type_name;        //aircraft full description eg: Boeing 767-300
    var $type_is_multi;    //if aircraft is multi-engine Y/N    
    var $type_is_complex;  //if aircraft is complex Y/N
    var $type_fixed_wing;  //if aircraft has fixed wings Y/N
    var $type_land;        //if aircraft operates over land Y/N/B B=Both
    var $pilot_id;
    
    const TABLE = "aircraft_type";
	
	/*Generates the insert sentence for the data instanciated in the structure
	 * @return string $query
	 * @author Daniel Waksman
	 * */
	function gen_SQL_insert(){
		$query = "INSERT INTO ".DB_NAME.".".$this::TABLE."(type_iata, type_name, type_is_multi, type_is_complex, type_fixed_wing, type_land, pilot_id) VALUES ('$this->type_iata','$this->type_name', '$this->type_is_multi' ,'$this->type_is_complex', '$this->type_fixed_wing','$this->type_land', '$this->pilot_id');";
		return $query;
	}
    
	/* Generates SQL sentence to delete the data instanciated in the structure
	 * @return string $query
	 * @author Daniel Waksman
	 * */
	function gen_SQL_delete(){
		$id = $this->type_id;
		$query = "DELETE FROM ".DB_NAME.".".$this::TABLE." WHERE type_id = $id;";
		echo $query;
		return $query;
	}	

	/* Insert the data instanciated in the structure into de database
	 * @author Daniel Waksman
	 * */
	function insert_yourself(){
		require_once 'methods.inc';
		$query = $this->gen_SQL_insert();
		execSQL_Insert($query);
	}
	
	/* Deletes an aircraft as the ID in the structure
	 * @author Daniel Waksman
	 * */
	function delete_yourself(){
		require_once 'methods.inc';
		$query = $this->gen_SQL_delete();
		execSQL_Insert($query);
	}
     
}


?>
<?php  

//Database
defined("DB_HOST") or define("DB_HOST", 'localhost');
defined("DB_NAME") or define("DB_NAME", 'logbook');
defined("DB_USER") or define("DB_USER", 'root');
defined("DB_PASS") or define("DB_PASS", 'root');

//Path Variables
defined("LIBRARY_PATH") or define("LIBRARY_PATH", realpath(dirname(__FILE__) . '/library'));     
defined("CLASS_PATH") or define("CLASS_PATH", realpath(dirname(__FILE__) . '/class'));
defined("BASE_PATH") or define("BASE_PATH", realpath(dirname(__FILE__) . '/../'));
defined("CSS_PATH") or define("CSS_PATH", realpath(dirname(__FILE__) . '/../public_html/css'));
defined("JS_PATH") or define("JS_PATH", realpath(dirname(__FILE__) . '/../public_html/js'));

//Name Variables
defined("CSS_MOVIL") or define("CSS_MOVIL", 'mobile.css');
defined("CSS_SCREEN") or define("CSS_SCREEN", 'desktop.css');
define('TIMEZONE', 'America/Montevideo');
  
//Include Path
ini_set("include_path",LIBRARY_PATH);

 
//Error Reporting  
ini_set("error_reporting", "true");  
error_reporting(E_ALL|E_STRCT);  
/*error_reporting(0);*/


//TIMEZONE
date_default_timezone_set(TIMEZONE);
?>  

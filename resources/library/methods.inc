<?php

	/* Tweak to redirect without common issues
	 * @param text $url: The Url to be redirected to
	 * @author Daniel Waksman
	 * */
	function redirect($url) {
    	header("Location: $url");
    	exit;
	}
	
	/* Connects to the database configured in the config.php global variables and performs a $query
	 * @param Text $query the SQL query to be exectued
	 * @returns $arr that can be navigated using 
	 * foreach($arr as $row) {
		 * echo $row['co1'];}
	 * @author Daniel Waksman
	 * */
	function execSQL ($query){
			
		$DBServer = DB_HOST; // e.g 'localhost' or '192.168.1.100'
		$DBUser   = DB_USER;
		$DBPass   = DB_PASS;
		$DBName   = DB_NAME;
		$conn = new mysqli($DBServer, $DBUser, $DBPass, $DBName);
		
		// check connection
		if ($conn->connect_error) {
		  trigger_error('Database connection failed: '  . $conn->connect_error, E_USER_ERROR);
		}
		
		$rs=$conn->query($query);
 
		if($rs === false) {
		  trigger_error('Wrong SQL: ' . $query . ' Error: ' . $conn->error, E_USER_ERROR);
		} else {
		  $arr = $rs->fetch_all(MYSQLI_ASSOC);
		  return $arr;
		}
		
		$conn->close();
	}

	/* Connects to the database configured in the config.php global variables and performs the insert given the query recieved
	 * @param Text $query the SQL query to be exectued
	 * @author Daniel Waksman
	 * */
	function execSQL_Insert ($query){
		
			
		$DBServer = DB_HOST;
		$DBUser   = DB_USER;
		$DBPass   = DB_PASS;
		$DBName   = DB_NAME;
		$conn = new mysqli($DBServer, $DBUser, $DBPass, $DBName);
		
		// check connection
		if ($conn->connect_error) {
		  trigger_error('Database connection failed: '  . $conn->connect_error, E_USER_ERROR);
		}
		
		$rs=$conn->query($query);
 
		if($rs === false) {
		  trigger_error('Wrong SQL: ' . $query . ' Error: ' . $conn->error, E_USER_ERROR);
		}
		
		$conn->close();
	}
	
?>